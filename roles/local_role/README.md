# Summary

##
This role is used to setup the following:

* Install portainer as a docker container

To do:

## Variables

| Variable name            | Description                                                       | Default value        |
|--------------------------|------------------------------------------------------------------------------------------|
| [app_name]_project_name  | Specifies the type of install. Typically either docker or server. |                      |
