# [Project Name]

This project hosts the configurations of the various tools hosted on the NuDay infrastructure

## Setup Instructions

* Download the git repository

* Update shared roles

  * Update initial submodules

  ```bash
  git submodule init && git submodule update
  ```

  * Subsequent updates to submodules

  ```bash
  git submodule update --remote --merge
  ```

* Create local password file for easy vault encryption and decryption

## To run a playbook, typical instructions are:

```bash
ansible-playbook [playbook name].yml -i inventories/ --ask-pass --ask-become-pass --vault-password-file local_configs/.vault_password.txt --tags="[tag_name_1 tag_name_2]"
```

Specific instructions will be stored in the individual playbook files


## Playbooks

| Playbook                   | Description                                          |
|----------------------------|------------------------------------------------------|
| setup_nuday_infrastructure | Sets up all the projects in the Nuday infrastructure |

## Projects

| Projects                   | Description                                          | Tags                    |
|----------------------------|------------------------------------------------------|-------------------------|
| firsttiger.com             | First Tiger Public Web Site                          | firsttiger.com          |
